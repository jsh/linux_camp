#notes for Lab "There is no place like home"

Steps to create and nfs on S2
notes "poweroff" or "shutdown -h 1" or "shutdown -h now" or 'init0'
1. shutdown S2
2. add a hardrive - 32GB
3. powermachine back up
4. ssh from workstation into s2 'ssh root@h2'
5. fdisk /dev/sdb 
  fdisk and parted are required for exams // lsblk lists disks in tree view
6. type 'o' for label msdos
7. 'p' create partition
8. '1' selects partition 1
9. 'enter key' this selects default starting key
10. '+4G' add 4 gigs from first sector of partition
11. w writes changes to disk and exits
12. lsblk to see the partitions after changes
13. mkfs.ext4 -L home /dev/sdb1 (missed the rest of this command)
  uuid is what is used to create a disk mount
14. blkid /ev/sdb1
15. blkid /etc/sdb1 >> /etc/fstab
16. edit fstab
  dump is the only program that respects the dump field in fstab
  final feild 0 means dont check 1 means check first 2 means check all 2's after 1 is done.
17. change line to UUID='' /home/ ext4 defaults 1 2
  there can be only one /home
18. ps -ef | grep -v root (will give you all procces and who owns them
19. umount /home
20. mount /home
21. fdisk -l too ensure
22. now remount old home mkdir /old_home
23. chattr +i /old_home/ 
  (this will make the filesystem readonly. This will prevent accident writting to the mount point)
24. mount /old_home/ /old_home
25. move files from /old_home to /home
26. init 3 (take out of single mode)
27. now back to ssh from workstation
  install nfs server
29. apt-get install nfs-kernel-server
  apt-file!!!

*** we had to go back and fix the portmapper

you may need to restart /etc/init.d/rpcbind

it may be required to run
update-rc.d rpcbind defaults
***

30. vi /etc/exports
  add /home *.garheade.sofree.us(rw,no_subtree_check)
    this gives permission to all of the machines on the garheade.sofree.us network
  go to s1 and mount
31. ssh to s1
32. mount -f nfs s2:/home /mnt
  this should return nfs error
33. now install nfs client
  yum install nfs-utils
34. now try line 32 again
35. vi /etc/fstab 
  register in fstab
36. s2:/home /home nfs noatime,hard,intr,acl 0 0
  change to single users, mark home as immutable, backup oldhomes
37. init1
38. cd /home
39. back up home to a tar file and move to roots home dir
40. now rm -rf * !!!! beware of where you run it
41. chattr +i /mnt/home
42. switch back to runlevel 3
43. now repeat from 31 on w1 (decided against doing in class.
  this will be slightly different since /home is a local mnt point instead of a seperate drive

