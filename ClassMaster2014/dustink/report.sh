#!/bin/bash


#report summary script

echo
echo
echo
#cpu info
echo "CPU info"
echo
echo
cat /proc/cpuinfo | grep -E "model name|cpu cores|processor" 
echo
echo


#memory
echo "memory"
echo
echo
cat /proc/meminfo | grep Mem
echo
echo


#total space
echo "total space"
echo
echo
df --out=size | head -2 | grep -v 1K-blocks


#disks
echo "disks"
echo
echo
df -h
echo
echo


#UUID
echo "UUID"
echo
echo
ls -l /dev/disk/by-uuid
echo
echo


#fstab info
echo "fstab info"
echo
echo
cat /etc/fstab
echo
echo


#wireless card
echo "wirless card"
echo
echo
lspci | grep 'Network controller'
echo
echo


#video card
echo "video card"
echo
echo
lspci | grep 'VGA'
echo
echo


#kernel
echo "kernel"
echo
echo
uname -a
echo
echo


#release info
echo "release info"
echo
echo
cat /etc/*release
echo
echo
