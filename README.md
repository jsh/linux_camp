Linux Camp
========

Welcome to SFS' Linux Camp git repo

* [ FLOSS Weekly Interview ]( https://youtu.be/QCD43LlKj24 )
* [ 2016 Linux Camp ]( http://www.sofree.us/linux-camp-2016/ )

This material is designed to create realistic scenarios to teach the skills to be a Linux System Administrator as defined by the LFCS exam.
By the end of Linux Camp, students will have worked through most, if not all, of these labs to explore the topics of the exam in a hands-on
environment.

The topics covered by this class material:

* Service Management
	- Start, Stop, Enable, Disable services without getting into details about any particular service
* Core GNU utilities
	- `sed`, `awk`, `vim`, etc.
* Network Settings and Management
* Package Management
* Managing Users and Groups
* File and Folder Permissions
* Scheduling
* Storage and File Systems Management
* Shell Scripting

[CC BY SA]( LICENSE )
