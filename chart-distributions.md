distributions
===

On 2016-07-30, these three are the LFCS exam distros.

You may want to use the upstream distros, too, to get new features as soon as possible.

* CentOS 7
  optional upstream: Fedora 24

* openSUSE 13.1
  optional upstreams:
  - openSUSE LEAP (newer)
  - openSUSE tumbleweed (newest)

* Ubuntu 14.04
  optional upstreams:
  - Ubuntu 16.04 (newer)
  - debian:
    - Jessie
    - Stretch
    - unstable
