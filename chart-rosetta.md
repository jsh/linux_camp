The distro Rosetta Chart
===

The package manager is:
- `rpm` in EL and SUSE
- `dpkg` in Debian'ish

The package repository manager is:
- `dnf` in Fedora
- `yum` in EL
- `apt` in Debian'ish, or `apt-get`, `synaptic`, or `aptitude`

To install a whole software stack, like LAMP:
- `yum groups install lamp-server` in EL
- `sudo zypper install -t pattern lamp_server` in SUSE
- `sudo tasksel install lamp-server` in Debian'ish

How do I Totally Update Everything?:
- CentOS 7:
  ```shell
  sudo yum clean all &&
  sudo yum -y upgrade
  ```
- Fedora 24:
  ```shell
  sudo dnf distro-sync
  ```
- openSUSE:
  ```shell
  sudo zypper dist-upgrade
  ```
- ubuntu 14.04:
  ```shell
  sudo apt update &&
  sudo apt dist-upgrade &&
  sudo apt autoremove
  ```

How do I search the package repositories
... for a binary, like ncdu, byobu, or git?
- CentOS 7:
  ```shell
  yum provides "*bin/byobu"
  ```
- openSUSE:
  ```shell
  cnf byobu
  ```
- Ubuntu:
  ```shell
  byobu
  # then just do what it tells you
  ```

How do I search the package repositories
... for another file, like the 'words' file?

How do I figure out which package owns /this/here/file?

How do I start, stop, enable, and disable daemons?
