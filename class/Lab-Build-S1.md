Lab: Build S1
-------------

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Please build a server with CentOS, minimum build. Call it S1 and for now add it to the hosts file on your workstation.

> It doesn't need much in terms of virtual hardware: 512MB memory and an 8GB hard drive should be fine.

> Make sure you can ssh into it from your workstation using "ssh some-user@s1"

> --Jan

---

---

You'll probably need to modify the ifcfg '-eth' or '-enp' files in /etc/sysconfig/network-scripts/ to make network interfaces start on boot.

--Kim

---

Suggested values:
- ISO: CentOS-7-x86_64-Minimal-1511.iso
- vm name: S1-CentOS-7
- hostname: S1.(student-project-name).MacGuffins.test
- Memory: 1GB
- Storage: 8GB
- Network: (match W1)

Notes:

Set the hostname and reboot.

Verify that you can ssh to the new server from your W1, at least.
```shell
ssh builder@192.168.134.201
```

Highly-motivated partners will ensure that they can ssh to one another's servers.

Hints
---

- nmtui
- hostnamectl
- /etc/sysconfig/network-scripts/ifcfg-*
- ip addr show (ip a)
