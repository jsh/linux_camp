Lab: Build S2
-------------

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Please build another server, but put Ubuntu Server on this one. Make sure to NOT name the first account alice, bob, or jan. 'builder' is fine, or 'tmpadmin', or whatever.

> And don't forget to confirm that you can ssh to it.

> --Jan

---

Notes:

Verify that you can ssh to the new server.You might need to modify /etc/network/interfaces to make network interfaces start on boot.

--Kim

---

Suggested values:
- ISO: ubuntu-14.04.4-server-amd64.iso
- vm name: S2-Ubuntu-14.04
- hostname: S2.(student-project-name).MacGuffins.test
- Memory: 1GB
- Storage: 8GB
- Network: (match W1)

Notes:

Set the hostname and reboot.

Verify that you can ssh to the new server from your W1, at least.
```shell
ssh builder@192.168.134.202
```

Highly-motivated partners will ensure that they can ssh to one another's servers.

Hints
---

- /etc/network/interfaces
- sudo ifdown eth0 ; sudo ifup eth0
- sudo service networking [stop|start|restart]
- hostnamectl
- ip addr show (ip a)
- sudo tasksel
- /etc/hosts
- sudo tee [-a]
