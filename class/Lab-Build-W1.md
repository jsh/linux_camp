Lab: Build W1
-------------

---

> FROM: Kim <kim@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Please build an openSUSE 13.1 VDI workstation / terminal-server to do your work from. Make sure to name the first user 'builder' or something else generic. Security wants the build users removed, but don't do that until after you create the user accounts.

> Try to setup something for screen-sharing over the network so you can pair over distance.

> --Kim

---

Notes
---

Feel free to use another distro, if you like; none of the labs have an extreme dependency on openSUSE, but this is the only openSUSE machine in the core set, and, depending on your experience level, you may want to stay with the distributions that your teacher(s) are familiar with.

You may want to install a vnc and/or xRDP servers so that you can easily get a remote graphical shell on this machine.

Suggested values:
- ISO: openSUSE-13.2-GNOME-Live-x86_64.iso
- VM name: W1-openSUSE-13.1
- hostname: W1.(student-project-name).MacGuffins.test
- hostname example: W1.alicorn.MacGuffins.test
- Memory: 2GB
- Storage: 16GB with LVM
- network: bridged if possible, NAT Network if bridged won't work
- verify guest additions are working:
  * desktop automatically resizes to window
  * shared clipboard should work
- Try a shared vnc/xRDP/byobu/tmux/screen session with your partner(s)

After install is complete, poweroff and start a "Linux Camp" group in VirtualBox. Dive into that group if you have a lot of visual clutter.


Hints
---

- hostnamectl

vnc:
- there is a built-in vnc-server: settings, system, sharing, screen sharing
- vino-server demands TLS
- vinagre supports TLS (TigerVNC and Remmina seem to not)

screen:
- some-ty1: `screen -S somename`
- some-ty2: `screen -x`

ssh:
- should be on, but may need to turn off firewall or poke a hole in it
