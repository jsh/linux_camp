Lab: Accessing the Commons
==========================

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> We have a 'commons' share where employees can easily post large or fast-changing files (rather than sending them in email). Please make sure that all machines have easy access to that.

> I want it to appear at /commons and every file of it should be writable from every machine for every user.

> Thanks for taking care of this,

> --Jan

---

> FROM: Kim <kim@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Yo A & B,

> Use autofs. It rocks. Automatically mounts needed things, and unmounts idle things. Don't hard-mount NFS over the WAN or I'll eat a kitten.

> This should be pretty easy and fast, I hope. Install autofs, enable the "/net -hosts" line in /etc/autofs.master, a symlink to /net/rackmaster/commons in the root...

> Remember to enable autofs or you'll wish you had, right around the time the first employee gets in, the morning after maintenance.

> --K

---

### Process Notes

Decide what you're going to call it: maltese-falcon, genesis-device, zombie-virus, briefcase, treasure-map, holy-grail, death-star-plans, rosebud, one-ring, or whatever

See also: how to do a lab

### Hints of commands and configs

- yum
- zypper
- apt

- systemctl
- service

- /etc/auto.master
