Lab: A little bit GUI, but not unpleasantly sticky...
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Can you get enough X and vnc onto the servers to make startx, ssh -X or ssh -Y, and vnc server to work properly for the few programs that need them, like Oracle Database setup.

> Also, I secretly hate vi*, *emacs, pico/nano, and I'll probably hate joe whenever I get around to trying it. Wouldn't it be nice to have a decent editor like gedit available?

> I *don't* want the servers to boot to a GUI login!

> Thanks as always,

> --Jan

---

Notes:

Requirements...

Servers must not boot into a GUI.

gedit, xeyes, and xclock can be launched from an ssh session to the server. The apps should display on the ssh client, the workstation.

Lowest package count wins!

If time allows...

* startx or “init 5” at the server-console brings up a usable desktop environment
* x11vnc can share the desktop. Create a listening vnc viewer on one machine, and push a display to it from another with “x11vnc --connect”
* vncserver works and presents a usable remote desktop
* setup an RDP server
