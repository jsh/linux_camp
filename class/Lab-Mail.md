Lab: Mail matters
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> We keep missing important logs and alerts. We should be seeing something in our inboxes every time a cron or at job fires. The way it's set up now, backups could start failing and we wouldn't know until we wanted to do a restore!

> Please configure the servers to send root's mail to root's local mail spool and to some real person, somewhere.

> Thanks,

> --Jan

Notes:

The line in /etc/aliases will go something like this:

    bob:   \bob,real-admin@real-domain.tld
    root:  \root,real-admin@real-domain.tld
    alice: \alice,real-admin@real-domain.tld

cron, at, and mail should work 'normally'. That is, the owner of any cron or at job should get mailed un-captured output and errors, and the mail program should be able to send mail. If you want to get fancy, you could try using uuencode to create a mail-attachment.

I recommend postfix or sendmail.

Don't forget to run newaliases after changing /etc/aliases.
