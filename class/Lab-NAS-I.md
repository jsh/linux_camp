Lab: NAS I: Better, Faster File-server
===

******************************

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> I'm getting lots of complaints from the art and rendering teams that the central NAS is too slow transferring assets over the WAN, especially for the offices with slow links.

> Please setup a NAS on your network for your team to use.

> Thanks for taking care of this,

> --Jan

******************************


### Process Notes

Consider checking /etc into git. It's a *great* way to roll-back undesired changes, and examine changes made by user-friendly GUI utilities, like YAST and system-config-*

Do this in parts:

1. Create LV-student-project-name
2. Create squashed NFS export
3. NFS mount it and/or autofs and symlink (everywhere)
4. Optionally, Samba share it (from S1) and add users
5. If you Samba-shared it, and you have Windows machines, test the share

Mount/remount of file-systems should be completed in maintenance mode (ie: when nobody but root is logged in) and/or when no files are open the affected file-system. Use 'lsof' 'fuser' and 'lslk' to check file-system use.

**LVM**

1. create a new logical volume named after your project
   Ex: lv-alicorn
2. create a file-system on the logical volume, optionally, label it
3. create a directory in /srv to which to mount the file-system to and mark it immutable
   Ex: /srv/alicorn
4. test-mount
   Ex: mount /dev/mapper/root-alicorn /srv/alicorn
4. register the file-system in /etc/fstab, by UUID
   Ex: blkid /dev/mapper/root-alicorn | sudo tee -a /etc/fstab &&
   grep alicorn /proc/mounts | sudo tee -a /etc/fstab &&
   vim /etc/fstab
5. umount the test and mount by registration
   Ex: sudo umount /srv/alicorn && sudo mount /srv/alicorn

**NFS Export**

Consider the export options on RackMaster:

```
[sfs@s1-00 ~]$ cat /etc/exports
/srv/commons *(rw,all_squash,no_subtree_check,insecure,anonuid=99,anongid=99)
```

- NFS servers depend on RPC / portmap. If you're hitting a long pause and everything is "right", make sure that RPC / portmap *and* the NFS kernel server are started.
- Enterprise Linux may start a firewall by default. If it does, tune it, or stop and disable it.
- Enterprise Linux enables SELinux by default. You may need to tune it or disable it.
- To check for firewall or selinux problems, turn them off and retry the operation.

1. Install nfs-server
2. Edit /etc/exports
3. start nfs-server and test mount the export on any other machine
4. enable nfs-server to start automatically

**NFS Mount (hard)**

WARNING: NFS hard-mounts queue I/O like a local drive. The writes are entirely uninterruptible, AFAICT, so if a mounted NFS device gets a write request, that request will wait forever, never erroring out, and preventing some kinds of operation. If the NFS server cannot be made available again, in a way that satisfies the NFS client, the system has to be power-cycled to clear pending writes.

The process for mounting s1:/srv/student-project-name on each client is something like this:

1. Create the /student-project-name directory and mark it immutable
2. Register the mount information in /etc/fstab
3. Test the registration ( by mounting /student-project-name )

**NFS automount**

1. Confirm that autofs is installed and started.
2. Confirm that the line `/net -hosts` in `/etc/auto.master` is active.
3. Symlink from /student-project-name to /net/s1/srv/student-project-name

**Samba Share (optional)**

Consider the share definition on RackMaster:

```
[commons]
	comment = Linux Camp / MacGuffins Shared Files
	path = /srv/commons
	read only = No
	guest only = Yes
	guest ok = Yes
```

1. Install Samba
2. Add a share-definition to /etc/samba/smb.conf
3. Add samba accounts for users that will want access over smb/cifs
   Ex: smbpasswd -a bob
3. Start the smb service, test and troubleshoot as necessary
5. Enable the service

**Samba clients**

* In Windows: add S1 to lmhosts and run \\s1\student-project-name
* In GNOME: From Files, click "Connect to Server" and open smb://s1/student-project-name
* You can also use smbclient or mount.cifs


### Hints on commands and configs

- cfdisk
- pvcreate
- vgextend
- lvcreate
- mkfs
- mount
- /etc/fstab

- yum
- showmount
- exportfs
- systemctl
- /etc/exports
- firewall-cmd
- getenforce
- setenforce
- chcon
- /etc/selinux/config

- smbstatus
- smbclient
- smbpasswd
- mount.cifs
- /etc/samba/smb.conf
