Lab: Install/configure NTP as-needed
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Would you check all the machines to make sure they're within a minute or two of each other, and configure an ntp server on one?

> --Jan

---
