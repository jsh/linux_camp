Lab: Configuring networking settings
------------------------------------

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice and Bob,

> I am just crazy old-fashioned and I'd really rather you put those servers on manually-configured static IP addresses, instead.

> Thanks!

> --Jan

---

static ip addresses. wow. crazy's right.

system-config-network is easy on enterprise linux, but make sure you check the ifcfg-blah files in /etc/sysconfig/network-scripts/ and /etc/sysconfig/network
on enterprise linux 7, use nmtui to get a basic setup, and don't forget to check /etc/hostname

on debian, just use the man page for interfaces and set the hostname in /etc/hostname

if you have anything else other than enterprise and debian, you're on your own

always, always double-check /etc/hosts

and keep the hosts entries in sync

--Kim

---

Notes:

 * nmtui
 * /etc/network/interfaces
 * /etc/hosts
 * /etc/resolv.conf

For each of your servers, configure eth0 with:

  * Static IP address
  * subnet mask

Confirm these settings survive the change:

  * gateway / router
  * nameserver / domain

Confirm that all the machines can still see each other. You will probably need to update hostlines.

Distribution-specific tips

 * Ubuntu (Desktop): Use the GUI interface to Network Manager
 * CentOS: nmtui or system-config-network, and/or edit /etc/sysconfig/network-scripts/ifcfg-blah, /etc/sysconfig/network, and /etc/hostname
 * OpenSUSE: Edit /etc/sysconfig/network/ifcfg-eth1 and /etc/HOSTNAME.
 * Debian/Ubuntu Server: Edit /etc/network/interfaces and /etc/hostname.

Verify all host-lines in /etc/hosts files on all machines follow this form:

    ip.address fully-qualified-name alias1 alias2 alias3
    For example:
    192.168.56.1   nb-dwillson-2 host
    192.168.56.11  w1.willson.sofree.us w1
    192.168.56.12  w2.willson.sofree.us w2
    192.168.56.21  s1.willson.sofree.us s1
    192.168.56.22  s2.willson.sofree.us s2

Confirm that each machine returns the correct FQDN to hostname -f and the correct short hostname to hostname -s

Use "ifconfig -a" and "ip addr show" and "route", at least.
