Lab: Cruddy server incident
===

(WIP - This shit is broken and in need of repair!)
---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> The website is down. One of our older servers went belly up. We found it under a desk of a former employee who departed two years ago. Don't worry about the multiple coffee stains. It won't boot at all, I don't think anyone knows how to log into it, and did I mention that THE WEBSITE IS DOWN? I need you to fix that server, and migrate the services off it ASAP.

> Thanks,

> --Jan

---

Notes:

Import 'crasher.ova' to VirtualBox, and do as many of the following as you can:

Make it boot-able (re-install grub mbr)

Log in (reset root password)

Figure out what distribution and version this is (/etc/*-release)

Transfer the data, NAS role, and web-server role to (an)other machine(s)

one-page web /var/www/html

files in /shared

samba configuration

---

Eval / Recovery / Crasher
===
- Tech Eval
  - Make evaluation machines and script
  - For whom the reboot tolls (tech eval)
  story: old crappy server, recently recovered, rebooting every hour, last tech hit by truck or lottery ticket
  - Do recovery/evaluation in teams
  - Move data from recovered to new machines?

crasher - prep it
---

* add accounts for dani, fred, gert
* add NFS share of web-site
* schedule hourly reboot
* reset root's password
* disable NFS

crasher - fix it
---

* The system should update and reboot *monthly* (not hourly).
* All users and admins should login with their *own* accounts.
  * Create accounts for alice, bob, and jan
  * Configure sudo
* Web-admins should not have interactive accounts.
  * change preferred shell for dani, fred, gert to /bin/false
  * confirm that dani, fred, gert can access NFS and sftp to update web-content

* Write evaluation routine: deliver at beginning and end of 'Camp.
  - How many cores does this computer have?
  - How much RAM does this computer have?
  - How many disks? Of what sizes?
  - Disable random_reboot
  - add users
  - disable users
  - add LV
  - schedule backups
  - mount NFS export
  - export NFS

  Setup:

  Configure evaluation images...

   * default minimum build
   * a cron-job:
      - after max_uptime  + ($RANDOM) minutes
      - patches fully
      - crushes root password
      - deletes root's authorized_keys
      - deletes any accounts from "default_list": ubuntu, admin, linux, etc...
      - reboots the box
      - logs all this

  Build caching squid proxy


  Evaluation:

  Story: "The new web-server is incorrectly configured. It patches and reboots every 45 *minutes* or so. It should patch and reboot every 45 *days* or so. While you're on, please do the following:"

  * break in/onto
  * confirm/add
  * set max_uptime to 30 days (not 30 minutes)
  * Move the machine to 10.73.73.(100+student_number)?
  * Instructor: This should be a proxied network.
  * git clone the website
