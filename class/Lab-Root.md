Lab: The unknown root password
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Someone (Eve?) changed the root password on S1. Can you reboot the box to single mode and reset the root password?

> --Jan

---

Notes:

On S1, run this as root to set the password to an unknown value and reboot

newpw=$( uuidgen )
# or
newpw=$( pwgen 32 1 )

echo -e "$newpw\n$newpw" | sudo passwd root
echo -e "$newpw\n$newpw" | sudo passwd me

reboot
