Lab: Setting up ssh keys
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Please generate password-protected ssh keys for yourselves, and setup authorized_keys files so that you can sign into either server without entering a password (other than the one you enter to unlock your keyring or your private key). Don't forget that .ssh/authorized_keys needs to be restrictively permissioned. chmod 600 does it, I think.

> --Jan

---

Notes: Install the public key on one server manually, and on the other using ssh-copy-id.
