Lab: Sudoers on all the things
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Let's not share the root password anymore. Yes, I know I was the one that said we should share it to begin with. I've learned!

> Anyway, make sure you both have your own accounts on all the machines, and membership in a group that's visible everywhere. I think you have 'linux_admins' everywhere and your UID's are in sync, right? If so, just add 'linux_admins' to sudoers. I'll leave it up to you whether or not to make it password-less, but I want you to be able to sudo everything everywhere.

> --Jan

---

Notes:

Now is a great time to set a good password on the 'alice' or 'bob' account on w1 and start using it for everything. For an extra challenge, remove all the other passwords or replace them with unknown values. To see who has passwords, view /etc/shadow as root.

 * visudo
 * /etc/sudoers
 * /etc/sudoers.d/
 * sudo -l -U gert
