Lab: (optional) Web matters
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> I'd like to stop paying (company) to host our web-site. You guys are plenty smart enough to do this.
> I'd like one of the servers to serve up 'dev' and 'prod' websites. The 'prod' should also come up on just the domain name, and 'www', of course.
> 
> Thanks,

> Jan

---

> FROM: Kim <kim@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,
>
> Don't worry so much about the content, if you can get a placeholder website we can have our interns add to it later.
> It's probably easier to get your webserver running first, and then work on configuring your hostnames.
> Make sure you can only see the webservers from outside the network
>
> Kim

---

Notes:

All the notes that apply to the [“Mail really matters” lab](class/Lab-Mail-O.md) apply equally to this one.
Configure additional hosts entries of 'dev.yourname.sofree.us', 'prod.yourname.sofree.us', and 'www.yourname.sofree.us' for the external IP in the server and all clients. Or, DNS.
Use your name as a sub-domain, and enter appropriate 
nmap the internal and external interfaces of the web-server. ssh should not be open on the “external” interface.
