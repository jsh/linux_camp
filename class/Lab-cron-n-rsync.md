Lab: Rsync and crontab
======================

> FROM: Kim <kim@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> The commons copy of your project doesn't show any progress from the last few weeks, but Dani just sent up a new draft of the manuscript, which Jan is head-over-heels for, so I know your people are working.

> I'm guessing you've got your team using a local NAS? If so, that's fine, but can you cron an rsync job so the commons copy stays up-to-date?

> Thanks,

> Kimmo

---

Drop a replication script in /etc/cron.hourly

The core of it should copy everything from your local-NAS-project-folder to your project-folder in the commons.

Something like this:
`sudo rsync -avHP /alicorn/ /commons/alicorn/`
Or this:
`sudo rsync -avHP --delete /alicorn/ /commons/alicorn/`
