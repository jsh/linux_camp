Pre-Lab: Install VirtualBox
---------------------------

Notes:

Please install VirtualBox on your physical box.

I've heard that VirtualBox installs on Windows and OSX, so if you currently have Windows or OSX and you don't want to fix it, that's fine, but you might not be able to play the Linux games I might recommend to you. :-)

You can find VirtualBox in the repositories for many Linux distributions, or you can download it from www.virtualbox.org.

Please make sure your default disk location for virtual machine files has LOTS of free space. 16 or 32 GB might be a reasonable minimum.


The instructor will have built a public NAS named ClassMaster. In the upcoming stories we'll refer to this server as RackMaster (doesn't
make much sense for Jan to be talking about a class server at work).

You may need to configure an entry for ClassMaster in hosts or lmhosts.
`192.168.134.1 ClassMaster.SFS.test ClassMaster`
or access ClassMaster by IP address.

Connect to ClassMaster from your physical host.
- nfs - classmaster:/srv/commons
- smb/cifs - \\classmaster\commons

Create a folder in the commons named after your MacGuffin, e.g.: alicorn, rage-virus, genesis-device, or whatever... That is your student-project-folder. In that folder, create a folder named 00.Pre-Lab. In *that* folder, put notes that you think may be helpful to your partners and/or other students. Finally, create an empty file named 'done'.

---

Although your virtual network / lab environment will not be fully populated until after the Build W1, S1, and S2 labs ...

... glance at the intended end result for your own lab environment:
  ![Student Network]( ../student-network.jpg )

... glance at the intended end result for the whole classroom/cabin:
  ![Class Network]( ../class-network.jpg )
