SFS Linux Camp Class Materials
===

*Linux Camp, by David L. Willson and the Software Freedom School, copyright license CC BY SA 3.0*

Linux Camp 2016 Staff
---

David Willson, Heather Willson, Troy Ridgley, and Aaron Brown.


Ordered Stories
---

[Introductions](Intro.md)

[Pre-Lab](Pre-Lab.md)

[Lab-Build-W1.md](Lab-Build-W1.md)

*** evaluation / crasher / recovery lab ***

[Lab-Build-S1.md](Lab-Build-S1.md)

[Lab-Build-S2.md](Lab-Build-S2.md)

*** Start [Lab-Maintenance](Lab-Maintenance.md) before turning in for the night and finish it in the morning. Do **not** start the big fat update during class! ***

[Lab-Commons](Lab-Commons.md)

[Lab-HardwareInventory.md](Lab-HardwareInventory.md)

[Lab-StorageInventory.md](Lab-StorageInventory.md)

[Lab-Users.md](Lab-Users.md)

[Lab-EmployeeHandbook.md](Lab-EmployeeHandbook.md)

[Lab-Root.md](Lab-Root.md)

[Lab-Sudoer.md](Lab-Sudoer.md)

[Lab-SSH.md](Lab-SSH.md)

[Lab-Network.md](Lab-Network.md)

[Lab-NAS-I.md](Lab-NAS-I.md)

[Lab-NAS-II.md](Lab-NAS-II.md)

[Lab-cron-n-rsync.md](Lab-cron-n-rsync.md)

[Lab-Doc-Share.md](Lab-Doc-Share.md)

[Lab-Scripts.md](Lab-Scripts.md)

[Lab-Mail.md](Lab-Mail.md)

[Lab-Later.md](Lab-Later.md)

[Lab-Schedule.md](Lab-Schedule.md)

[Lab-The-Sam-Hammer](Lab-The-Sam-Hammer.md)

[Lab-Evil-Eve.md](Lab-Evil-Eve.md)

[Lab-GUI.md](Lab-GUI.md)

[Lab-NTP.md](Lab-NTP.md)


[Lab-Recover.md](Lab-Recover.md)

As Time Allows
---

[Lab-Print.md](Lab-Print.md)

[Lab-Mail-O.md](Lab-Mail-O.md)

[Lab-Home.md](Lab-Home.md)
Kill this exercise! Don't ever do this, even if you have time! It is a Bad Idea!

[Lab-Web-O.md](Lab-Web-O.md)

[Lab-Wordpress-O.md](Lab-Wordpress-O.md)

[Lab-cdir-O.md](Lab-cdir-O.md)

[Lab-Alien-O.md](Lab-Alien-O.md)

[Lab-web-db-O.md](Lab-web-db-O.md)

[Lab-Shell-O.md](Lab-Shell-O.md)
