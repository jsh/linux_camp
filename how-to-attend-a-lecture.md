How to attend a lecture
===

Consider taking advantage of the reliability and power of paper and pens.

Watch and listen. Take pictures and/or record video if you can do so without breaking attention.

Write down important strategic ideas and tactical details. Be pictorial. Clarity matters, but neatness does not.

Only try things during lecture if you can do so without breaking attention.
