How to do a lab
===

Go and sit close to your parter(s).

Generally, one partner should operate their console, while the other partner(s):
- Takes notes (and pictures and drawings)
- Helps with research on hard questions
- Oversees, advises, questions, and encourages

Share the screen. Use a secondary monitor, vnc, screen, or something, if needed, so advising partners can see clearly what the operating partner is doing.

Create a team folder in the Commons if you haven't already.

Create one folder in your team folder for each lab.

We should all use the same format for the folder-name for each lab; we want a convention that will sort nicely, maybe $( date --iso=minutes )-word-...-word

Put a copy of *everything* related to that lab in that folder: ideas, scripts, data-files, notes, screenshots, pictures of hand-written notes, inspirational sayings, insights, and reminders of things you want to research later

When your team has either completed the lab and you are ready for the next lecture, touch a file in the lab-folder named...

Done (or 00-done or done or ZZ-done or any variant of done, just so long as you flag your done-ness)

definition-of-done:

For each member in team, member can say something like: "I have completed the lab to the point that my solution meets all expressed requirements. I've asked enough questions that I understand what I did. I took enough notes that I could do it again or show someone else how to do it. I refilled my drink or let off a little water, and I'm ready to learn something else."
