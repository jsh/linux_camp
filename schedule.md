2016 Schedule
===

Father's Day, June 19th
- Early Bird discount ends

Independence Day, July 4th
- Early PIF (paid-in-full) discount ends

Friday, August 19th
- Linux Campers and Staff: Please come up Friday afternoon/evening, if you can.
- 6PM: Check in with the front desk and get a map to the cabin.
- All pack-in, claim rooms/bunks, FCFS!
- Staff sets up gear, tape-down, put away food, etc.
- 8PM'ish: Games, movie, campfire, etc...

Saturday, August 20th
- 8'ish: breakfast
- 09:00: Training begins
- Show CheatSheet
- [pleases](https://gitlab.com/sofreeus/sofreeus/blob/master/pleases.md)
- Introductions: For each new camper, get the enroller and send/queue a spiff
- Pick Partners
- Do evaluation, swap/rotate, and check
- 6PM'ish: Dinner, games, movie, campfire, etc...
- Big update before sack-out (NOT during class!)

Sunday, August 21st
- 8'ish: breakfast
- 09:00: Training resumes
- 6PM'ish: Dinner, games, movie, campfire, etc...

Monday, August 22nd
- 8'ish: breakfast
- 09:00: Training resumes
- 6PM'ish: Dinner, games, movie, campfire, etc...

Tuesday, August 23rd
- 8'ish: breakfast
- 09:00: Training resumes
- Linux Camp After-Party scheduling
- LFCS Activities
  + exam registrations - See "Linux Camp Private" for discount code
  + review topics - Use Session 0 from LFCS CSG
- Review CheatSheet
- Evaluation
-
- 6PM'ish: Dinner, games, movie, campfire, etc...

Wednesday, August 24th
- 8'ish: breakfast?
- by 10:00: cabin is reset and empty
