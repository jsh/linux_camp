The Story
=========

Bob and Alice are SA's for a movie MacGuffin project being produced by MacGuffins.

MacGuffins: Quality plot-devices, combining mystery with inscrutability since early the 20th century.

Thanks for joining the MacGuffins network. May your story be a prosperous one, but only after a period of intense struggle and/or suspense of the perfect duration and intensity.


values for labs
---------------

student-project-name should be chosen by the student

examples: alicorn (from Legend), maltese-falcon (from The Maltese Falcon), genesis-device (from The Wrath of Khan), rage-virus (from 28 Days Later)

CEO: Jan
CTO: Kim
CSO: Sam
