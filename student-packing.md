Packing List for Students
===

**comfort** - chair, blanket, pillow

**clothing** - for cool and warm weather

**laptop** ( for BYOB's ) - 2+ cores, 4+ GiB memory, 8+ GiB storage, VirtualBox

**notebook/sketchbook** - note-taking / drawing supplies

**toiletries**

 - toothbrush and toothpaste
 - deodorant
 - shampoo
 - soap
 - towel

**toys**

 - swimsuit
 - mountain bike
 - hiking shoes
 - game/movie
 - molecule(s) - caffeine, alcohol, nicotine, whatev...
