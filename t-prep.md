Teacher Prep
===

40 hours or so each year

Review and revise all the materials
Do all the labs twice (like two students)
Plan for classroom delivery: make sufficient notes that good ideas are not lost, but not so much that mediocre notes bury good notes.

Build images for recovery scenario in all LFCS exam distributions.

Build ClassMaster
